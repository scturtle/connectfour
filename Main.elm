import Array exposing (..)
import Array.Extra as E
import Text as T
import Color exposing (..)
import Graphics.Collage exposing (..)
import Graphics.Element exposing (..)
import Graphics.Input exposing (..)

type alias Table = Array (Array Int)
type alias Model = {
    turn : Int -- 0: even, 1: player1, 2: player2, -1: player1 win, -2: player2 win
  , table : Table
  }

initState : Model
initState = { turn = 1, table = repeat 7 (repeat 6 0)}

type Action = Click (Int, Int) | Noop

clickChannel : Signal.Mailbox Action
clickChannel = Signal.mailbox Noop

update : Action -> Model -> Model
update action model =
  case action of
    Noop -> model
    Click (i, j) ->
  if model.turn <= 0 -- game has ended
  then initState else
    if not <| canPut (i, j) model.table
    then model -- cannot put there, nothing changed
    else let newtable = put (i, j) model.turn model.table
         in { model | turn = checkWin model.turn newtable
                    , table = newtable }

canPut : (Int, Int) -> Table -> Bool
canPut (i, j) table =
  let at (i, j) table = E.getUnsafe j <| E.getUnsafe i table
  in at (i, j) table == 0 && (j == 0 || at (i, j-1) table /= 0)

put : (Int, Int) -> Int -> Table -> Table
put (i, j) v table = E.update i (E.update j (always v)) table

checkWin : Int -> Table -> Int
checkWin curr table =
  let and = foldr (&&) True
      or = foldr (||) False
      drop = E.sliceFrom
      test4 a b c d = a == curr && b == curr && c == curr && d == curr
      testRow a = or <| E.map4 test4 a (drop 1 a) (drop 2 a) (drop 3 a)
      winRow = or <| map testRow table
      testCol a b c d = or <| E.map4 test4 a b c d
      testDiag1 a b c d = or <| E.map4 test4 a (drop 1 b) (drop 2 c) (drop 3 d)
      testDiag2 a b c d = or <| E.map4 test4 (drop 3 a) (drop 2 b) (drop 1 c) d
      winCol = or <| E.map4 (\a b c d -> testCol a b c d || testDiag1 a b c d || testDiag2 a b c d)
                            table (drop 1 table) (drop 2 table) (drop 3 table)
  in if winRow || winCol then -curr {-win-}
     else if and <| map (and << map (\v -> v /= 0)) table then 0 {-even-}
     else 3-curr {-opposite-}

genCircle : Color -> Element
genCircle color =
  container 70 70 middle <| collage 50 50 [filled color <| circle 25]

toColor : Int -> Color
toColor v =
  if v == 0 then grayscale 0.1 else if v == 1 then green else red

showTurn : Int -> Element
showTurn turn =
  container 490 70 middle <|
    let description =
          collage 130 50 [text <| T.height 40 <| T.fromString <|
            if turn > 0 then "'s turn" else
            if turn == 0 then "even" else " wins"]
    in if turn == 0
       then description
       else genCircle (toColor <| abs turn) `beside` description

stateToView : Model -> Element
stateToView model =
  above (showTurn model.turn) <|
    container 490 420 middle <|
      flow right <| toList <| flip indexedMap model.table <| \i row ->
        flow up  <| toList <| flip indexedMap row <| \j v ->
          genCircle (toColor v)
            |> clickable (Signal.message clickChannel.address <| Click (i, j))

main : Signal Element
main =
  Signal.map stateToView <|
    Signal.foldp update initState clickChannel.signal
